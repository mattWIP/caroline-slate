/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.Header = (function() {

  var selectors = {
    hamburger: '.hamburger-container',
    mobile: '[data-mobile-hamburger]',
    body: 'body'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function Header(container) {
    this.$container = $(container);
    $(selectors.mobile).on("click", this.drawerOpen);
  }

  Header.prototype = $.extend({}, Header.prototype, {

    /**
     * Updates the DOM state of the add to cart button
     *
     * @param {boolean} enabled - Decides whether cart is enabled or disabled
     * @param {string} text - Updates the text notification content of the cart
     */

     drawerOpen: function(evt){
          let drawerScreen = $('<div>', { class: 'drawer_screen' });
          $(selectors.body).addClass('drawer-left__open');
          $(selectors.body).append(drawerScreen);
     },
    /**
     * Event callback for Theme Editor `section:unload` event
     */
    onUnload: function() {
      this.$container.off(this.namespace);
    }
  });

  return Header;
})();
