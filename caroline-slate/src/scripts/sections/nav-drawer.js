/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */

theme.Drawer = (function() {

  var selectors = {
    expandButtons: '.expand',
    close: '[data-nav-drawer] .drawer__header .icon-close',
    body: 'body',
    screen: '.drawer_screen'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function Drawer(container) {
    this.$container = $(container);

    $(selectors.expandButtons).on("click", this.expandMenus);
    $(selectors.close).on("click", this.closeDrawer);
    $(selectors.screen).on('click', this.closeDrawer);
  }

  Drawer.prototype = $.extend({}, Drawer.prototype, {

    /**
     * Updates the DOM state of the add to cart button
     *
     * @param {boolean} enabled - Decides whether cart is enabled or disabled
     * @param {string} text - Updates the text notification content of the cart
     */

    closeDrawer: function(el){
        $(selectors.body).removeClass('drawer-left__open');
        $(selectors.screen).remove();
    },

    expandMenus: function(el){
        console.log(el.target);
        let target = $(el.target);

        target.closest('.has-submenu').toggleClass('expand-menu');
    },

    /**
     * Event callback for Theme Editor `section:unload` event
     */
    onUnload: function() {
      this.$container.off(this.namespace);
      console.log('load this section dammit');
    }
  });

  return Drawer;
})();
