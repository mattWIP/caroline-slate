/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.slider = (function() {

  var selectors = {
    container: '[data-slider-container]',
    body: 'body',
    slides: '.slide',
    imgs: '.slide_img'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function slider(container) {
    let GlobalThis = this;
    this.$container = $(container);
    this.srcSwap();

    $(selectors.container).on('init', this.showSlides);

    this.$container.imagesLoaded()
      .always( function( instance ) {
        console.log('all images loaded');
      })
      .done( function( instance ) {
        GlobalThis.initSlick();
      })
      .fail( function() {
        console.log('all images loaded, at least one is broken');
      })
      .progress( function( instance, image ) {
        var result = image.isLoaded ? 'loaded' : 'broken';
        console.log( 'image is ' + result + ' for ' + image.img.src );
      });
  }

  slider.prototype = $.extend({}, slider.prototype, {
    /** 
     * Event callback for Theme Editor `section:unload` event
     */
    srcSwap: function(){
        console.log('lets load secondary items');
        $(selectors.imgs).each(function(i){
            let img = $(this);
            let srcToBe = img.data('src');
            if( i > 0){
                img.hide();
                img.attr('src', srcToBe);
            }
        });
    },

    initSlick: function(){
        $(selectors.container).slick({
          fade: true,
          arrows: false,
          autoplay: true,
          zIndex: 2,
          infinite: true
        });
    },

    showSlides: function(){
        $(selectors.imgs).each(function(){
            $(this).show();
        });
    },

    onSelect: function(){
        },

    onDeselect: function(){
      
    },

    onUnload: function() {
      this.$container.off(this.namespace);
    }

  });

  return slider;
})();
