/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.collection = (function() {

  var selectors = {
    container: '[data-collection-container]',
    grid: '[data-collection-grid]',
    grid__item: '[data-collection-grid-item]',
    trigger_item: '[data-collection-grid-item]:last-child',
    body: 'body',
    mobileFilter: '[data-filter-mobile-button]',
    mobileFilterContainer: '[data-mobile-filter-container]',
    lazyLoad: '[data-lazy-load]',
    needsFade: '[data-needs-fade]',
    pagination: '.pagination',
    loadIcon: '.loader',
    current: 'span.page.current'
  };

  var timeout = false;
  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */


  function collection(container) {
    let GlobalThis = this;
    this.$container = $(container);

    this.lazyLoad();
    $(selectors.grid).on('handlebared', this.postHandlebar);
    $(selectors.mobileFilter).on("click", this.showFilters);
    $(window).on("scroll", function(){
        if( GlobalThis.ajaxTrigger($(selectors.trigger_item)) && !timeout){
            console.log('trigger hit');
            let url = $(selectors.current).next().find('a');
            url = url.attr('href');
            $(selectors.loadIcon).show();
            timeout = true;
            GlobalThis.gatherProducts(url, GlobalThis.callback);
        }
    });
  }

  collection.prototype = $.extend({}, collection.prototype, {
    /** 
     * Event callback for Theme Editor `section:unload` event
     */

    ajaxTrigger: function(elem){
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + ($(elem).height() * .5);

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    },

    postHandlebar: function(){
        $(selectors.needsFade).imagesLoaded()
            .always( function( instance ) {
             // console.log('all images loaded');
            })
            .done( function( instance ) {
             // console.log('all images successfully loaded');
            })
            .fail( function() {
             // console.log('all images loaded, at least one is broken');
            })
            .progress( function( instance, image ) {
              var result = image.isLoaded ? 'loaded' : 'broken';
              // console.log( 'image is ' + result + ' for ' + image.img.src );
              // console.log(image);
              var parent = $(image.img).parent().parent();

              parent.fadeIn();
            });
    },

    callback: function(json){
        console.log(json);
        let products = json.products
        let replacementURL = products[0].nextURL;
        $(selectors.current).next().find('a').attr('href', replacementURL);
        // Handlebars.js cart layout
        
        var items = [],
            item = {},
            data = {},
            source = $("#CollectionGrid").html(),
            template = Handlebars.compile(source);
        
            $.each(products, function(index, product){
              // Check if image exists for product
              // console.log(index);
              // console.log(product);
       
              if(product.images != null){
                // Use Slate to get a properly sized image
                      var prodImg = product.images[0].src;
                      prodImg = slate.Image.getSizedImageUrl(prodImg, '1024x1024');
                  }else{ // No image.
                      var prodImg = "//cdn.shopify.com/s/assets/admin/no-image-medium-cc9732cb976dd349a0df1d39816fbcc7.gif";
                  }
              var price = (( product.price > product.price_min ) ? product.price_min : product.price );
              var price = slate.Currency.formatMoney(price, theme.moneyFormat);
             
              // Create item data then push to items again
              item = {
                available: product.available,
                body_html: product.body_html,
                collections: product.collections,
                handle: product.handle,
                id: product.id,
                featured_img: prodImg,
                secondary_img: (( product.images[1] ) ? product.images[1].src : prodImg),
                price: price,
                type: product.product_type,
                tags: product.tags,
                title: product.title,
                url: product.url,
                vendor: product.vendor
              };

              items.push(item); 
            });
               // Gather all cart data and add to DOM
              data = {
                items: items
              }

              $(selectors.grid).append(template(data)).trigger('handlebared');

              if(replacementURL == 'null' ){
                  console.log('null link');
              }else{
                setTimeout(function(){
                  timeout = false;
                  $(selectors.loadIcon).hide();
                }, 1000);
              }


    },

    gatherProducts: function(url, callback){
      let newURL = url + '&view=json';
        //console.log(newURL);
        $.get(newURL, function(response) {
              //console.log(response);
              response = JSON.parse(response);
              callback(response);
              // let formattedMoney = $(slate.Currency.formatMoney(response.products[0].price, theme.moneyFormat));
              // console.log(formattedMoney);
        });
    },

    lazyLoad: function(){
        console.log('start lazy load');
        $(selectors.lazyLoad).each(function(){
          let img = $(this);
          let newSRC = img.data('src');

          img.attr('src',newSRC);
          if(img.hasClass('ge-img1')){
              img.show();
          }
        });
    },

    onSelect: function(){
        },

    onDeselect: function(){
      
    },

    onUnload: function() {
      this.$container.off(this.namespace);
    },

    showFilters: function(){
      console.log('show filters');
        let container = $(selectors.mobileFilterContainer);

        if( container.hasClass('showFilters') ){
            $(selectors.mobileFilter).text('Filter');
        }else{
            $(selectors.mobileFilter).text('X');
        }
        $(selectors.mobileFilterContainer).toggleClass('showFilters');
    }


  });

  return collection;
})();
