/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */

theme.lookbook = (function() {

  var selectors = {
    body: 'body',
    lookbookItem: '[data-lookbook-item]',
    lookbookContainer: '[data-lookbook-container]',
    nextBtn: '.next.lightbox_button',
    prevBtn: '.prev.lightbox_button',
    btn: '.lightbox_button',
    lightbox: '.lightbox',
    lightboxInner: '.lightbox_inner',
    lightboxImg: '.lightbox_img',
    lightboxProduct: '.lightbox_product',
    productImg: '.lightbox_product-img',
    productTitle: '.lightbox_product-title',
    productLink: '.lightbox_product-link',
    displayProduct: '.display_product'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function lookbook(container) {
    this.$container = $(container);

    this.createLightbox();

    $(selectors.lookbookItem).on('click', this.fill);
    $(selectors.lightbox).on('click', this.exit);
    $(selectors.btn).on('click', this.btnClickHandler);
    $(selectors.displayProduct).on('click', this.productToggle);
  }

  lookbook.prototype = $.extend({}, lookbook.prototype, {

    /**
     * Updates the DOM state of the add to cart button
     *
     * @param {boolean} enabled - Decides whether cart is enabled or disabled
     * @param {string} text - Updates the text notification content of the cart
     */

    exit: function(evt){
        let target = $(evt.target);

        if( target.hasClass('lightbox') ){
            target.removeClass('filled showProduct hasProduct');
            lookbook.prototype.removeFocus();
        }
    },

    btnClickHandler: function(evt){
        let target = $(evt.target);
        let fillTarget = target.attr('target');

        lookbook.prototype.indexFill(fillTarget);
    },

    productToggle: function(evt){
        const currentText = $(selectors.displayProduct).text();
        let nextText = $(selectors.displayProduct).data('text');

        $(selectors.displayProduct).text(nextText);
        $(selectors.displayProduct).data('text', currentText);
        $(selectors.lightbox).toggleClass('showProduct');
    },

    fill: function(evt){
        let item = $(this);
        let index = item.index();
        console.log(index);
        lookbook.prototype.indexFill(index);

    },

    indexFill: function(index){
        let item = $(selectors.lookbookItem).eq(index);
        let total = $(selectors.lookbookItem).length;
        let product = item.data('product');
        let prevIndex = ((index-1 < 0 ) ? total-1 : index-1);
        let nextIndex = ((index+1 > total) ? 0 : index+1);
        let img = item.find('img').attr('src');
        let productInfo = {};

        if( product !== undefined){
          // Determine if info needs to be pulled or if element contains the data.

            if( item.hasClass('data') ){
                // Data has already been pulled, get it from element.
                let productInfo = {
                  img: item.data('product-img'),
                  title: item.data('product-title'),
                  url: item.data('product-url')
                }
                    $(selectors.lightbox).addClass('hasProduct');
                    $(selectors.productImg).attr('src', productInfo.img);
                    $(selectors.productTitle).text(productInfo.title);
                    $(selectors.productLink).attr('href',productInfo.url);
                
            }else{
                // Data has not been pulled yet, ajax, attach it here & to element.
              let newURL = '/products/' + product + '?view=json';
              let responsed = null;
              $.get(newURL, function(response) {
                    response = JSON.parse(response);
                    console.log(response);
                    productInfo = {
                      img: response.images[0].src,
                      title: response.title,
                      url: response.url
                    }
                    item.attr({
                      'data-product-img': productInfo.img,
                      'data-product-title': productInfo.title,
                      'data-product-url': productInfo.url
                    }).addClass('data');

                    $(selectors.lightbox).addClass('hasProduct');
                    $(selectors.productImg).attr('src', productInfo.img);
                    $(selectors.productTitle).text(productInfo.title);
                    $(selectors.productLink).attr('href',productInfo.url);
              });
            }
        }else{
          $(selectors.lightbox).removeClass('hasProduct');
        }

        $(selectors.lightboxImg).attr('src',img);
        $(selectors.nextBtn).attr('target', nextIndex);
        $(selectors.prevBtn).attr('target',prevIndex);
        $(selectors.lightbox).addClass('filled');
        lookbook.prototype.trapfocus();
    },

    trapfocus: function(){
        slate.a11y.trapFocus({
          $container: $(selectors.lightbox),
          namespace: 'lightbox',
          $elementToFocus: $(selectors.lightboxInner)
        });
    },

    removeFocus: function(){
        slate.a11y.removeTrapFocus({
          $container: $(selectors.lightbox),
          namespace: 'lightbox'
        });
    },

    createLightbox: function(){
        let box = $('<div>', { class: 'lightbox' });
        let inner = $('<div>', { class: 'lightbox_inner' });
        let productContainer = $('<div>', { class: 'lightbox_product' });
        let img = $('<img>', { class: 'lightbox_img' });
        let productImg = $('<img>', { class: 'lightbox_product-img' });
        let productTitle = $('<span>', { class: 'lightbox_product-title' });
        let productLink = $('<a>', { class: 'lightbox_product-link' });
        let productBtn = $('<button>', { class: 'display_product' });
        let nextBtn = $('<button>', { class: 'next lightbox_button' });
        let prevBtn = $('<button>', { class: 'prev lightbox_button' });

        productBtn.attr('data-text', 'Show Image');
        productBtn.text('Show Product');
        productLink.text('Add To Cart');

        productContainer.append(productImg, productTitle, productLink);
        inner.append(productBtn, nextBtn, img, prevBtn, productContainer);
        box.append(inner);
        box.appendTo($(selectors.body));
    },

    /**
     * Event callback for Theme Editor `section:unload` event
     */
    onUnload: function() {
      this.$container.off(this.namespace);
      console.log('load this section dammit');
    }
  });

  return lookbook;
})();
