/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.cart = (function() {

  var selectors = {
    container: '[data-cart-drawer]',
    body: 'body',
    cartContainer: '[data-cart-drawer]',
    cartIcons: 'a[href*="/cart"]',
    closeDrawer: '[data-cart-header] svg',
    cartBody: '[data-cart-body]'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function cart(container) {
    this.$container = $(container);
    let greater = $(container).parent();
    let cartIcons = $(selectors.cartIcons);
    let cartClose = $('button#cartClose');

    $(selectors.closeDrawer).on("click", this.close);
    $(selectors.cartIcons).on("click", this.open);


    jQuery(document.body).on('afterCartLoad.ajaxCart', function(evt, cart) {
        this.open
    });
  }

  cart.prototype = $.extend({}, cart.prototype, {
    /** 
     * Event callback for Theme Editor `section:unload` event
     */

    onSelect: function(){
      this.open();
    },

    onDeselect: function(){
      this.close();
    },

    onUnload: function() {
      this.$container.off(this.namespace);
    },



    open: function(evt){
      if(evt){
        evt.preventDefault();
      }
      
      $(selectors.body).toggleClass('drawer-right__open');
      slate.a11y.trapFocus({
        $container: $('.cart_drawer'),
        namespace: 'drawer'
      });
    },

    close: function(evt){
      if(evt){
        evt.preventDefault();
      }
      $('body').toggleClass('drawer-right__open');

      slate.a11y.removeTrapFocus({
        $container: $('.cart_drawer'),
        namespace: 'drawer'
      });       
    }

  });

  return cart;
})();
