/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.celebrity = (function() {

  var selectors = {
    container: '[data-celebrity-grid]',
    item: '[data-celebrity-item]',
    body: 'body',
    lightbox: '.lightbox',
    lightboxImg: '.lightbox_img',
    productBox: '.lightbox_product',
    productImg: '.lightbox_product-img',
    productSpan: '.lightbox_product-title',
    productLink: '.lightbox_product-link',
    displayProduct: '.display_product',
    lastChild: '.celebrity-item:last-child',
    pagination: '.pagination',
    paginationNext: '.next a'
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function celebrity(container) {
    this.$container = $(container);
    var globalThis = this;
    this.createLightbox();
    $(selectors.item).on('click', this.clickHandler);
    $(selectors.lightbox).on('click', this.exit);
    $(selectors.displayProduct).on('click', this.handleShow);

    $(window).on("scroll", function(){
          if( globalThis.ajaxTrigger($(selectors.lastChild)) && !$(selectors.body).hasClass('loading') && $(selectors.pagination).length > 0 ){
              globalThis.infinitePull();
          }
    });
  }

  celebrity.prototype = $.extend({}, celebrity.prototype, {
    /** 
     * Event callback for Theme Editor `section:unload` event
     */
    ajaxTrigger: function(elem){
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + ($(elem).height() * .5);

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    },

    pullCallback: function(posts){
          let length = posts.length;

          for(var j = 0; j < length ; j++ ){
                let individ = $(posts[j]);
                let delay = j * 100;
                individ.hide();
                setTimeout(function(){
                  individ.appendTo($(selectors.container));
                  individ.fadeIn();
                }, delay);

                if(j == length - 1){
                    celebrity.prototype.doneLoading();
                }
          }
    },

    doneLoading: function(){
        $(selectors.body).removeClass('loading');
    },

    infinitePull: function(){
        $(selectors.body).addClass('loading');
        let url = $(selectors.paginationNext).attr('href');
        console.log(url);

        $.get(url, function(response) {
              let $html = $(response);
              let items = $html.find('.celebrity-item');
              let next = $html.find('.pagination .next a');
              
              if(next.length > 0 ){
                  $(selectors.paginationNext).attr('href', next.attr('href') );
              }else{
                $(selectors.pagination).remove();
              }
              celebrity.prototype.pullCallback(items);        
        });
    },

    exit: function(evt){
        let target = $(evt.target);

        if( target.hasClass('lightbox') ){
            target.removeClass('filled showProduct hasProduct');
            celebrity.prototype.removeFocus();
        }
    },

    handleShow: function(){
        const currentText = $(selectors.displayProduct).text();
        let nextText = $(selectors.displayProduct).data('text');

        $(selectors.displayProduct).text(nextText);
        $(selectors.displayProduct).data('text', currentText);
        $(selectors.lightbox).toggleClass('showProduct');
    },

    trapfocus: function(){
        slate.a11y.trapFocus({
          $container: $(selectors.lightbox),
          namespace: 'lightbox',
          $elementToFocus: $(selectors.lightboxInner)
        });
    },

    removeFocus: function(){
        slate.a11y.removeTrapFocus({
          $container: $(selectors.lightbox),
          namespace: 'lightbox'
        });
    },

    _getIndex: function(elem){
        let index = elem.index();

        return index;
    },

    clickHandler: function(evt){
          let target = $(evt.target).closest('[data-celebrity-item]');
          let position = celebrity.prototype._getIndex(target);

          celebrity.prototype.fill(position);

    },

    _filterAttributes: function(elem){
        let attributeList = elem[0].attributes;
        let length = attributeList.length;
        let attributeArray = [];

        for(var v = 0; v < length; v++){
            let key = attributeList[v].name;
            let value = attributeList[v].nodeValue;

            if( key.indexOf("product") > -1 ){
                attributeArray.push(value);
            }
        }

        return attributeArray;
    },

    productFill: function(array){
      console.log(array);
        $(selectors.productImg).attr('src', array.src_img);
        $(selectors.productLink).attr('href', array.src_url);
        $(selectors.productSpan).text(array.src_title);
    },

    _filterProductAttributes: function(elem){
        let attributeList = elem[0].attributes;
        let length = attributeList.length;
        let attributeArray = [];

        for(var v = 0; v < length; v++){
            let key = attributeList[v].name;
            let value = attributeList[v].nodeValue;

            if( key.indexOf("src") > -1 ){
                attributeArray.push(value);
            }
        }

        return attributeArray;  
    },


    fill: function(index){
        let target = $(selectors.item).eq(index);
        let imgSrc = target.find('img').attr('src');
        let handles = celebrity.prototype._filterAttributes(target);
        if(handles.length === 0 ){
          // no products. Open lightbox with just image.
          $(selectors.lightboxImg).attr('src',imgSrc);
          celebrity.prototype.openBox();
        }else{
            // Check if this product data has already been pulled. 

            if( target.hasClass('data') ){ // Pull Data from Element
              let productData = celebrity.prototype._filterProductAttributes(target);
              console.log(productData);
              let set = {
                'src_url': productData[0],
                'src_img': productData[1],
                'src_title': productData[2]
              }
              celebrity.prototype.productFill(set);
              $(selectors.lightboxImg).attr('src',imgSrc);
              $(selectors.lightbox).addClass('hasProduct');
              celebrity.prototype.openBox();
            }else{ // Pull Data via Ajax

                for(var k = 0; k < handles.length; k++){
                   let handle = handles[k];
                   let newURL = '/products/' + handle + '?view=json';
                   let productSet = [];

                  $.get(newURL, function(response) {
                        response = JSON.parse(response);
                        productSet = {
                           'src_url': response.url,
                           'src_img': response.images[0].src,
                           'src_title': response.title
                        }
                        target.attr(productSet).addClass('data');
                        celebrity.prototype.productFill(productSet);
                        $(selectors.lightboxImg).attr('src',imgSrc);
                        $(selectors.lightbox).addClass('hasProduct');
                         
                  });

                  if( k == handles.length -1 ){
                      celebrity.prototype.openBox(); 
                  }

                } // end of forloop

            }// end of if else for checking where product data is
        }// end of if else for handles.length


    },

    openBox: function(){
          $(selectors.lightbox).addClass('filled');
          celebrity.prototype.trapfocus();
    },

    createLightbox: function(){
        let box = $('<div>', { class: 'lightbox' });
        let inner = $('<div>', { class: 'lightbox_inner' });
        let productContainer = $('<div>', { class: 'lightbox_product' });
        let img = $('<img>', { class: 'lightbox_img' });
        let productImg = $('<img>', { class: 'lightbox_product-img' });
        let productTitle = $('<span>', { class: 'lightbox_product-title' });
        let productLink = $('<a>', { class: 'lightbox_product-link' });
        let productBtn = $('<button>', { class: 'display_product' });
        let nextBtn = $('<button>', { class: 'next lightbox_button' });
        let celebrityName = $('<span>', { class: 'lightbox-name'});
        let prevBtn = $('<button>', { class: 'prev lightbox_button' });

        productBtn.attr('data-text', 'Show Image');
        productBtn.text('Show Product');
        productLink.text('Shop This Product');

        productContainer.append(productImg, productTitle, productLink);
        inner.append(productBtn, nextBtn, img, prevBtn, celebrityName, productContainer);
        box.append(inner);
        box.appendTo($(selectors.body));
    },

    onSelect: function(){
    
    },

    onDeselect: function(){
    
    },

    onUnload: function() {
      // this.$container.off(this.namespace);
    }

  });

  return celebrity;
})();
